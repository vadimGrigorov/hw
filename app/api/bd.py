from flask import request, jsonify, url_for
from app.models import BD_point
from app.api import bp
from app import db
from app.api.errors import bad_request

@bp.route('/BD/<int:id>', methods=['GET'])
def get_point(id):
    # Возвращаем пользователю об оборудовании с данным уникальным серийным номером
    # Если такового нет, возвращаем HTTP 404
    return jsonify(BD_point.query.get_or_404(id).to_dict())

@bp.route('/BD', methods=['GET'])
def get_points():
    # Какую страницу показать? По умолчанию 1
    page = request.args.get('page', 1, type=int)
    # Сколько элементов на странице? По умолчанию 10
    # Но не больше 100
    per_page = min(request.args.get('per_page', 10), 100)
    # Генерируем набор данных для страницы
    data = BD_point.to_collection_dict(BD_point.query, page, per_page, 'api.get_points')
    # Возвращаем пользователю json
    return jsonify(data)

@bp.route('/BD', methods=['POST'])
def create_point():
    # Проверяем, что запрос пришел с телом
    data = request.get_json() or {}
    # Если тела запроса нет, возвращаем ошибку
    if not data:
        return bad_request('Point should contain something')
    # Создаем экземпляр модели ORM для новой записи
    point = BD_point()
    # Загружаем данные из тела запроса в экземпляр
    point.from_dict(data)
    # Добавляем запись к текущей сессии с БД 
    db.session.add(point)
    # Комиттим транзакцию в БД
    db.session.commit()
    # По стандарту мы должны вернуть объект
    # с присвоенным уникальным идентификатором
    response = jsonify(point.to_dict())
    # Так же по стандарту код ответа должен быть 201 вместо 200
    # 200 OK
    # 201 Created
    response.status_code = 201
    # В заголовке передаем ссылку на созданный объект
    response.headers['Location'] = url_for('api.get_point', id=point.id)
    return response

@bp.route('/BD/<int:id>', methods=['PUT'])
def update_point(id):
    point = BD_point.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('Point should contain something')
    point.from_dict(data)
    db.session.commit()
    return jsonify(point.to_dict())

@bp.route('/BD/<int:id>', methods=['DELETE'])
def remove_point(id):
    point = BD_point.query.get_or_404(id)
    db.session.delete(point)
    db.session.commit()
    return jsonify(point.to_dict())